#!/bin/bash

rm book/$1/*.yaml
rm $2

for recipe in book/$1/*; do
	echo $recipe
	./digest.pl $recipe "${recipe%.html}.yaml"  >> $2
done

