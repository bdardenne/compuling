#!/usr/bin/perl

# Group 25
# A. Ancion & B. Dardenne
# INGI2263 Computational Linguistics
# Universite Catholique de Louvain
# 2015-2016


use strict;
use warnings;
use feature qw(say);  #DEBUG

open(my $INPUT, $ARGV[0]) or die "Could not open $ARGV[0] : $!";
open(my $OUTPUT, '>:encoding(UTF-8)', $ARGV[1]) or die "Could not open $ARGV[1] for writing : $!";
my $counter = 0;
my $debug = 0;

say $ARGV[0] if $debug;


## REGEXES

# 'Amount' is numbers with possible non-word characters  (, . / -     etc.) inbetween
# Also matches html entities (UTF-8 fractions etc) and some regular words

my $number = qr/(?:[½⅓¼⅕⅙⅛⅔⅖¾⅗⅜⅘⅚⅝⅞\d] | &[^\s]+; | 
	(?:one|two|three|four|five|six|seven|eight|nine|ten|evelen|twelve|half|quarter|third))/xi;

my $symbol = qr/[\-  \/  \.  \,  \+ \[ \] \s \\]/xi;

my $amount = qr/\W*((?: ${number}+ ${symbol}*)+)  /xi;

# Units 

# Avoid questions like "Can I eat this with..." but keep can as in 1 can of coke
my $pronouns = qr/(?:\s(?:i|you|s?he|we|they|this|it|that|u)\b)/i;
my $cans = qr/cans?(?!${pronouns})/i;   # negative lookahead, 'can' cannot be followed by a pronoun

my $unit = qr/\W*(tb?sp?|tb(?:ls)?|k?g|(?:fl\W+)?oz|ounces?|grams?|cups?|teaspoons?|tablespoons?|pkgs?|packets?|packages?|zests?|p?inch(?:es)?|C|ml|t|lbs?|(?:small|large|big)? box|pints?|sticks?|pounds?|${cans})\b\.?/xi;

my $html = qr/<.+?>/;


## SEARCH

while(<$INPUT>){

	# clean up parentheses
	$_ =~ s/\(.*\)//g;

	# 'While' to capture all matches on a same line if several ingredients on one line 
	while(m/<\w+ .*? itemprop=\"ingredient .*?>${html}* ${amount}?  \s* ${html}* ${unit}? ${html}* (?:\s*of|\s*from)? \s* ([a-z0-9].+?)<\/\w*?>/xgi){
		parse($1, $2, $3);
	}
}

exit 0 if $counter > 0;

seek $INPUT, 0, 0;

while(<$INPUT>){

	# Performance trick
	$_ =~ s/^\s+//g;

	# clean up parentheses
	$_ =~ s/\(.*\)//g;

	while(m/(?:^|<.+?>) ${amount}? \s* ${unit} (?:\sof\s|\sfrom\s)? \s* ([a-z0-9].+?) (?:<.*>|$) /xgi){
		parse($1, $2, $3);
	}
}

sub parse {

		my ($qty, $u, $clean) = @_;

		# Cleaning up potential html tags within ingredient
		$clean =~ s/<\/?.*?>//g;
		
		# Trim whitespaces and convert to lowercase
		$clean = lc(escape(trim($clean)));  
			
		# Drop ingredients ending with ':'  (likely a title...)
		# Or too long ingredients (likely a false positive)
		if((substr($clean, -1) eq ':') or (length($clean) > 70) or !$clean){
			return;
		}
		
		print $OUTPUT "- {";		
		
		if ($clean) {
			print $OUTPUT "ingredient: \"$clean\"";
			say "$clean";# if $debug;
			$counter++;
		}

		if($qty){
			$qty = lc(escape(trim($qty)));
			print $OUTPUT ", quantity: \"$qty\"";
			say "$qty" if $debug;
		}
		
		if($u){
			$u = lc(escape(trim($u)));
			print $OUTPUT ", unit: \"$u\"";
			say "$u" if $debug;
		}
		
		print $OUTPUT "}\n";
}

# Escape characters for YAML and remove non-printable characters (not accepted by YAML)
sub escape { my $s = shift; $s =~ s/([\"\\])/\\$1/g; $s =~ s/[^[:print:]]//g; return $s};
sub  trim { my $s = shift; $s =~ s/^\s+|\s+$//g; return $s };

